# ATLAS ops overview
  * [Massive blacklisting](./massive_blacklisting/)
  * [Self-approve a test](./self_approve_test/)
  * [Check dataset cache](./check_ds_cache/)
  * [Add/remove site to/from template](./add_site_to_template/)
  * [Debug template](./atlas-ops/debug_template_commissioning_test_submission/)