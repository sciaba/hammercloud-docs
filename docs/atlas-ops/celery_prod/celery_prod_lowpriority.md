# HammerCloud extension - `celery_prod_lowpriority` mode

In order to submit jobs with low priority (`-10`), use the `celery_prod_lowpriority` mode. 
For the rest, follow instructions for [`celery_prod` mode](celery_prod.html).