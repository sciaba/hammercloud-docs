# Data for Blacklisting study
Get data for blacklisting study in a json format, with script: 

* Last N days:
   
```
# ssh jschovan@hammercloud-ai-XY
# sudo su
# ACTION=ndays
# NDAYS=7 ### provide an integer of number of days, to get "last N days" worth of data
# /data/hc/apps/atlas/scripts/utils/blacklisting_study_data.sh $ACTION $NDAYS
```

* Start date / end date:
   
```
# ssh jschovan@hammercloud-ai-XY
# sudo su
# ACTION=interval
# STARTDATE=2016-10-01 ### provide start date, in format '%Y-%m-%d' 
# ENDDATE=2016-10-01 ### provide end date, in format '%Y-%m-%d' 
# /data/hc/apps/atlas/scripts/utils/blacklisting_study_data.sh $ACTION $STARTDATE $ENDDATE
```


# Howto

```
# /data/hc/apps/atlas/scripts/utils/blacklisting_study_data.sh


########## USAGE ##########
Usage:     blacklisting_study_data ndays $NDAYS
or:        blacklisting_study_data interval $STARTDATE $ENDDATE

e.g.:

    blacklisting_study_data.sh ndays 30

    blacklisting_study_data.sh interval 2016-10-01 2016-10-14

########## END OF USAGE ##########
```
