# Manual whitelisting

It is sometimes necessary to manually whitelist a panda queue,
e.g. when hammercloud is disabled on a queue but the status remains
TEST.

For manual whitelisting one can use the following on any hammercloud submit node. Needs read permissions for the proxy file!
```
# export X509_CERT_DIR=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/etc/grid-security-emi/certificates
# export X509_USER_PROXY=/data/hc/apps/atlas/config/x509rprod.celery

# curl -S --cert $X509_USER_PROXY --key $X509_USER_PROXY --cacert $X509_USER_PROXY --capath $X509_CERT_DIR 'https://atlas-agis-api.cern.ch/request/pandaqueuestatus/update/set_probestatus/?json&panda_resource=PANDAQUEUENAME&value=ONLINE&reason=set.online&probe=hammercloud'

# curl --cert $X509_USER_PROXY --cacert $X509_USER_PROXY --capath $X509_CERT_DIR "https://atlas-cric.cern.ch/api/atlas/pandaqueuestatus/update/set_probestatus/?json&pandaqueue=PANDAQUEUENAME&value=ONLINE&reason=set.online&probe=hammercloud"
```
