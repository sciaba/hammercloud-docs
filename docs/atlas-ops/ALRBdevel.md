# ALRBdevel tests

## Introduction 

ALRBdevel tests were introduced to HammerCloud in November 2018 to improve development and testing cycle of ``asetup`` and its dependencies. 
 
##  Components

### HammerCloud
HammerCloud submits ALRBdevel tests similar to PFTs/AFTs (tests used for blacklisting). ALRBdevel tests are NOT used for blacklisting. 

* Overview: Table ``Running and Scheduled ALRBdevel Tests`` on the main HC page: [http://hammercloud.cern.ch/hc/app/atlas/#ALRB](http://hammercloud.cern.ch/hc/app/atlas/#ALRB)
    * Overview per ATLAS Site: ``ALRBdevel`` option on the Overview - Site page, e.g. [http://hammercloud.cern.ch/hc/app/atlas/siteoverview/?site=CERN-PROD&startTime=2019-01-17&endTime=2019-01-22&templateType=ALRBdevel](http://hammercloud.cern.ch/hc/app/atlas/siteoverview/?site=CERN-PROD&startTime=2019-01-17&endTime=2019-01-22&templateType=ALRBdevel)

* Production ALRBdevel tests differ from PFTs in ``prodSourceLabel=rc_alrb``, ``processingType=gangarobot-alrb``, are NOT golden, have ``period=3600``, and ``type=ALRBdevel``. 
    * Monitoring: [https://bigpanda.cern.ch/jobs/?produsername=gangarbt&processingtype=gangarobot-alrb&prodsourcelabel=rc_alrb](https://bigpanda.cern.ch/jobs/?produsername=gangarbt&processingtype=gangarobot-alrb&prodsourcelabel=rc_alrb)
     
* Analysis ALRBdevel tests differ from AFTs in ``prodSourceLabel=rc_alrb``, ``processingType=gangarobot-alrb``, are NOT golden, have ``period=3600``, and ``type=ALRBdevel``. 
    * Monitoring: [https://bigpanda.cern.ch/jobs/?produsername=gangarbt&processingtype=gangarobot-alrb&prodsourcelabel=rc_alrb](https://bigpanda.cern.ch/jobs/?produsername=gangarbt&processingtype=gangarobot-alrb&prodsourcelabel=rc_alrb)
    
### Pilot wrapper
Pilot wrapper [https://github.com/ptrlv/adc/blob/master/runpilot3-wrapper.sh](https://github.com/ptrlv/adc/blob/master/runpilot3-wrapper.sh) takes care of setting the proper environment for Pilot. As of January 2019, these are the main changes to setup proper environment for ALRBdevel testing: 

```
  if [[ ${PILOT_TYPE} = "ALRB" ]]; then
     log 'PILOT_TYPE=ALRB, setting ALRB env vars to testing'
     export ALRB_asetupVersion=testing
     export ALRB_xrootdVersion=testing
     export ALRB_davixVersion=testing
     export ALRB_rucioVersion=testing
   fi
```

### Submit pilot wrappers: APF and Harvester
Since November 2018 there is a pilot factory that submits ALRBdevel pilot wrappers: [http://apfmon.lancs.ac.uk/aipanda124-alrb](http://apfmon.lancs.ac.uk/aipanda124-alrb). As of January 2019, there should be 1 pilot wrapper per PanDA queue submitted each 10 minutes. ``TODO``: many APFs geographically distributed, and Harvester. 

Since January 2019 there is a (devel) Harvester node that submits ALRBdevel pilot wrappers: ``test_fbarreir``. Frequency ``TODO``. 

### Pilot
Pilot recognizes 
 
 * option ``-i ALRB``
 * ``prodSourceLabel: rc_alrb``

### PanDA
PanDA recognizes HC jobs with ``prodSourceLabel: rc_alrb`` (and directs them to the correct pilot expecting ALRBdevel payload).


##  Troubleshooting

### No jobs
When there are 0 jobs total per any running test in [http://hammercloud.cern.ch/hc/app/atlas/#ALRB](http://hammercloud.cern.ch/hc/app/atlas/#ALRB), there is something wrong on the HC side, please contact <atlas-adc-hammercloud-support@cern.ch> to investigate.

### Only 1 job per PanDA queue per test
If HC managed to submit at least 1 job per PanDA queue per test, but there are not more jobs, and the jobs get stalled in ``activated`` job status, it suggests that HC part is working fine, and there is a problem with submission of pilot wrappers (APF or Harvester), or a problem with letting pilot know about the correct environment & payload. 

For the submission issues please contact 

 * APF: P. Love / <atlas-project-adc-operations-pilot-factory@cern.ch>
 * Harvester: Fernando/FaHui/<atlas-adc-harvester-central-support@cern.ch>
 

For the environment issues, please check with 
 
* Peter for pilot wrapper config, 
* Paul for pilot, 
* Tadashi/Fernando for PanDA.

### ALRBdevel tests are running, but in wrong environment 

If the ALRBdevel test jobs are running, but the jobs have wrong environment, please check with

* Peter for pilot wrapper config, 
* Paul for pilot, 
* Tadashi/Fernando for PanDA.

Example of wrong environment:

Check first what testing versions are; eg on lxplus, do
```
setupATLAS
showVersions asetup davix rucio xrootd | grep -e versions -e testing
```

Next, in the pilot logs (and athena.out file if applicable), check the variable
ALRB_requestedVersion has these testing versions inserted (ie first in the list in case of multiple setups).


e.g. these jobs not using ``ALRB_asetupVersion=testing``: https://bigpanda.cern.ch/job?pandaid=4216824140 since the asetup testing version at this time should have been 01-00-20.
```
ALRB_requestedVersions=cmake:3.11.0  
rcsetup:00-04-18  
acm:0.1.17 
asetup:V00-08-05  
davix:0.7.1-x86_64-slc6 xrootd:4.8.5-x86_64-slc6 
rucio:1.18.5 
emi:3.17.1-1.el6umd4v5  
rcsetup:00-04-18  
acm:0.1.17 
asetup:V00-08-05
```

### ALRBdevel job fails because of missing ``default``/project name

Reject the RC of ``asetup``, it is not safe to deploy due to backwards incompatibility, notify Shuwei Ye / the ``asetup`` team.

If such a RC made it to production, jobs may fail. At the 300k+ cores simultaneously. 


### Analy ALRBdevel jobs picked up by standard ``test`` pilot (missing ALRBdevel environment)
If the Analy ALRBdevel jobs are picked up by standard ``test`` pilot (missing ALRBdevel environment), the jobs need to have ``processingType=gangarobot-alrb`` and not ``processingType=gangarobot``. Tadashi/Paul/Peter can advise. 

 * Example of a job with wrong environment: [https://bigpanda.cern.ch/job?pandaid=4219009979](https://bigpanda.cern.ch/job?pandaid=4219009979) where pilot args were:

```
pilot.py -d /home/pilatlas003/condorg_J0JKIgxw -s ANALY_INFN-T1_TEST_SL7 -h ANALY_INFN-T1_TEST_SL7 -p 25443 -w https://pandaserver.cern.ch -u user -i PR -G 1

2019-01-21 04:41:04| 29214|pilot.py    | New job has prodSourceLabel=rc_alrb
```
 * Example of a job with a correct environment: [https://bigpanda.cern.ch/job?pandaid=4225888888](https://bigpanda.cern.ch/job?pandaid=4225888888).

```
# condor stdout
2019-01-28 15:29:39| 2727|pilot.py    | argParser arguments: ['-d', '/pool/condor/dir_20214/tmp/condorg_ZT3U4L6y', '-s', 'ANALY_CERN', '-h', 'ANALY_CERN', '-p', '25443', '-w', 'https://pandaserver.cern.ch', '-u'
, 'user', '-i', 'ALRB', '-i', 'ALRB', '-G', '1']

# pilotlog.txt
'ALRB_requestedVersions': 'davix:0.7.1-x86_64-slc6 xrootd:4.8.5-x86_64-slc6 rucio:1.18.8 emi:3.17.1-1.el6umd4v5 rcsetup:00-04-18  acm:0.1.17 asetup:V01-00-21

```

### ALRBdevel jobs fail because of too many cores (error 252):
The number of cores a job has to run with can be set in the `.tpl` file of a template via `job.coreCount`. If this is set to
```
job.coreCount = ###CORECOUNT###
```
HC will take whatever corecount value is defined for a certain PQ in AGIS, except for AFT/PFT jobs which have by default 1 core. This may cause problems if non-AFT/PFT tests run are with clones of AFT/PFT templates, especially when the job processes (for testing reasons) a very small number of events.
This can be quickly fixed by adding
```
SET[job.coreCount]=1
```
in the `Extraargs` field of the online template definition, meaning to overwrite the corecount value of the `.tpl` file.
Example:
* template 1034 is the ARLB clone of PFT template 841 (`Sim_tf.py`, `Atlas-19.2.3`, `--maxEvents=2`)
* corresponding tests are [20130820](https://hammercloud.cern.ch/hc/app/atlas/test/20130820/) (PFT, 1 core) and [20130825](https://hammercloud.cern.ch/hc/app/atlas/test/20130825/) (ARLB, 8 cores)
* [20130820](https://hammercloud.cern.ch/hc/app/atlas/test/20130820/) runs sucessfully over the 2 events
* [20130825](https://hammercloud.cern.ch/hc/app/atlas/test/20130825/) fails in AthenaMP merging step

