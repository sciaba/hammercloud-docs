# Add a site to a template
Add a site to a template, with script: 

```
# ssh jschovan@hammercloud-ai-XY
# sudo su
# ACTION=add
# TEMPLATEID=FIXME ### provide ID of a template, e.g. 852
# PRLIST=CERN-PROD,CERN-PROD-preprod ### provide a comma-separated list of PanDA resource names
# /data/hc/apps/atlas/scripts/server/add-site-to-template.sh $ACTION $TEMPLATEID $PRLIST

```

# Remove a site from a template
Remove a site from a template, with script: 

```
# ssh jschovan@hammercloud-ai-XY
# sudo su
# ACTION=remove
# TEMPLATEID=FIXME ### provide ID of a template, e.g. 852
# PRLIST=CERN-PROD,CERN-PROD-preprod ### provide a comma-separated list of PanDA resource names
# /data/hc/apps/atlas/scripts/server/add-site-to-template.sh $ACTION $TEMPLATEID $PRLIST

```


# Remove all sites from a template
Remove all sites from a template, with script: 

```
 # ssh jschovan@hammercloud-ai-XY
 # sudo su
 # ACTION=remove_all
 # TEMPLATEID=FIXME ### provide ID of a template, e.g. 852
 # PRLIST=blah ### provide a comma-separated list of PanDA resource names
 # /data/hc/apps/atlas/scripts/server/add-site-to-template.sh $ACTION $TEMPLATEID $PRLIST

```

# Copy all sites from a template to another template
Copy all sites from a template to another template, with script: 

```
 # ssh jschovan@hammercloud-ai-XY
 # sudo su
 # ACTION=copy
 # TEMPLATEID=FIXME ### provide ID of the destination template, e.g. 852
 # PRLIST=FIXME ### provide ID of the source template, e.g. 756
 # /data/hc/apps/atlas/scripts/server/add-site-to-template.sh $ACTION $TEMPLATEID $PRLIST

```

# Howto
```
 # /data/hc/apps/atlas/scripts/server/add-site-to-template.sh 


 Usage: add_site_to_template.py ACTION TEMPLATEID PanDA_resource_names
	where: ACTION is [add|remove|remove_all]
	       TEMPLATEID is ID of the template
	       PanDA_resource_names is a comma-separated list of PanDA resource names
	e.g.: python add_site_to_template.py    add           841    CERN-PROD-preprod               ### to add CERN-PROD-preprod to template 841
	e.g.: python add_site_to_template.py    remove        841    CERN-PROD-preprod               ### to remove CERN-PROD-preprod from template 841
	e.g.: python add_site_to_template.py    add           841    CERN-PROD-preprod,CERN-PROD     ### to add CERN-PROD-preprod and CERN-PROD to template 841
	e.g.: python add_site_to_template.py    remove_all    841    all                             ### to remove all sites from template 841
	e.g.: python add_site_to_template.py    copy          841    765                             ### to copy all sites from template 765 to template 841

```
