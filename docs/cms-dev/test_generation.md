# Test generation

**Test generation:** from admin template and .tpl file the job configuration files (files jobs/*.py) are created.

## Test generation in ``celery_crab3api``
* Main script: [apps/cms/python/scripts/submit/test_generate_celery_crab3api.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/test_generate_celery_crab3api.py)

  * main function: ``TestGenerateCelery.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/cc7/hc_core/scripts/submit/test-run.sh#L134)

* Related scripts: 
  * ``from atlas.python.scripts.submit.configmaker import ConfigMaker`` [apps/cms/python/scripts/submit/configmaker_celery_crab3api.py](https://gitlab.cern.ch/hammercloud/hammercloud-cmscrab3/-/blob/cc7/apps/cms/python/scripts/submit/configmaker_celery_crab3api.py) ... generate configuration, handle masks
