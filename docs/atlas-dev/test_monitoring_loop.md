# Test monitoring loop

**Monitoring loop:** monitoring loop keeps an eye on submitted/running jobs, and makes sure that desired number of jobs is running at each site (and submits more jobs once the running ones complete). At the end of the test request to cancel non-complete submitted/running jobs is issued. 

## Test monitoring loop in ``celery_prod``
* Main script: [apps/atlas/python/scripts/runtest_celery_prod.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/runtest_celery_prod.py)

  * main function: ``RuntestCeleryProd.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/master/hc_core/scripts/submit/test-run.sh#L56)

* Related scripts: 
  * ``from atlas.python.scripts.submit.jobstate import JobState`` [apps/atlas/python/scripts/submit/jobstate.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/jobstate.py) ... library to get job status info, map it to HC internal fields (``Result`` class)
  * ``from atlas.python.scripts.submit.job_status_map import job_status_map`` [apps/atlas/python/scripts/submit/job_status_map.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/job_status_map.py)
  * ``from atlas.python.scripts.submit.configmaker import ConfigMaker`` [apps/atlas/python/scripts/submit/configmaker.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/configmaker.py) ... generate configuration, handle masks


## Test monitoring loop in ``celery_prod_jedi``
* Main script: [apps/atlas/python/scripts/runtest_celery_jedi.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/runtest_celery_jedi.py)

  * main function: ``RuntestCeleryJedi.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/master/hc_core/scripts/submit/test-run.sh#L74)

* Related scripts: 
  * ``from atlas.python.scripts.submit.jobstate import JobState`` [apps/atlas/python/scripts/submit/jobstate.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/jobstate.py) ... library to get job status info, map it to HC internal fields (``Result`` class)
  * ``from atlas.python.scripts.submit.job_status_map import job_status_map`` [apps/atlas/python/scripts/submit/job_status_map.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/job_status_map.py)
  * ``from atlas.python.scripts.submit.configmaker import ConfigMaker`` [apps/atlas/python/scripts/submit/configmaker.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submit/configmaker.py) ... generate configuration, handle masks
